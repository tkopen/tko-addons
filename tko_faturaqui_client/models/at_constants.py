# Reference: Portaria n.o 302/2016, de 2 de dezembro
# http://info.portaldasfinancas.gov.pt/pt/informacao_fiscal/legislacao/diplomas_legislativos/Documents/Portaria_302_2016.pdf

at_tax_accounting_bases = {
    'C': 'Contabilidade',
    'E': 'Faturação emitida por terceiros',
    'F': 'Faturação',
    'I': 'Contabilidade integrada com a faturação',
    'P': 'Faturação parcial',
    'R': 'Recibos (a)',
    'S': 'Autofaturação',
    'T': 'Documentos de transporte (a)',
}

at_indicators = [
    ('0', 'No'),
    ('1', 'Yes'),
]

at_tax_types = {
    'IVA': 'Imposto sobre o valor acrescentado',
    'IS': 'Imposto do selo',
    'NS': 'Não sujeição a IVA ou IS',
}

at_tax_regions = {
    'PT': 'Portugal continental',  # ADD OTHER COUNTRIES?
    'PT-AC': 'Região Autónoma dos Açores',
    'PT-MA': 'Região Autónoma da Madeira',
}

at_tax_codes = {
    'RED': 'Taxa reduzida',
    'INT': 'Taxa intermédia',
    'NOR': 'Taxa normal',
    'ISE': 'Isenta',
    'OUT': 'Outros',
    'NS': 'Não sujeição',
    'NA': 'Nos recibos emitidos sem imposto discriminado',
}

at_source_billings = {
    'P': 'Documento produzido na aplicação',
    'I': 'Documento integrado e produzido noutra aplicação',
    'M': 'Documento proveniente de recuperação ou de emissão manual',
}

at_invoice_types = {
    'FT': 'Fatura, emitida nos termos do artigo 36.o do Código do IVA',
    'FS': 'Fatura simplificada, emitida nos termos do artigo 40.o do Código do IVA',
    'FR': 'Fatura-recibo',
    'ND': 'Nota de débito',
    'NC': 'Nota de crédito',
}

at_invoice_statuses = {
    'N': 'Normal',
    'S': 'Autofaturação',
    'A': 'Documento anulado',
    'R': 'Documento de resumo doutros documentos criados noutras aplicações e gerado nesta aplicação',
    'F': 'Documento faturado',
}

at_product_types = {
    'P': 'Produtos',
    'S': 'Serviços',
    'O': 'Outros',
    'E': 'Impostos Especiais de Consumo',
    'I': 'Impostos, taxas e encargos parafiscais',
}

at_payment_types = {
    'RC': 'Recibo emitido no âmbito do regime de IVA de Caixa (incluindo os relativos a adiantamentos desse regime)',
    'RG': 'Outros recibos emitidos',
}

at_payment_statuses = {
    'N': 'Recibo normal e vigente',
    'A': 'Recibo anulado',
}

at_source_payments = {
    'P': 'Recibo produzido na aplicação',
    'I': 'Recibo integrado produzido noutra aplicação',
    'M': 'Recibo proveniente de recuperação ou de emissão manual',
}

at_payment_mechanisms = {
    'CC': 'Cartão crédito',
    'CD': 'Cartão débito',
    'CH': 'Cheque bancário',
    'CI': 'Crédito documentário internacional',
    'CO': 'Cheque ou cartão oferta',
    'CS': 'Compensação de saldos em conta corrente',
    'DE': 'Dinheiro eletrónico, por exemplo residente em cartões de fidelidade ou de pontos',
    'LC': 'Letra comercial',
    'MB': 'Referências de pagamento para Multibanco',
    'NU': 'Numerário',
    'OU': 'Outros meios aqui não assinalados',
    'PR': 'Permuta de bens',
    'TB': 'Transferência bancária ou débito direto autorizado',
    'TR': 'Títulos de compensação extrassalarial independentemente do seu suporte, '
          'por exemplo, títulos de refeição, educação, etc.',
}

at_movement_types = {
    'GR': 'Guia de remessa',
    'GT': 'Guia de transporte (incluir aqui as guias globais)',
    'GA': 'Guia de movimentação de ativos fixos próprios',
    'GC': 'Guia de consignação',
    'GD': 'Guia ou nota de devolução',
}

at_movement_statuses = {
    'N': 'Normal',
    'T': 'Por conta de terceiros',
    'A': 'Documento anulado',
    'F': 'Documento faturado, ainda que parcialmente, quando para este documento também existe na tabela 4.1. – '
         'Documentos comerciais a clientes (SalesInvoices) o correspondente do tipo fatura ou fatura simplificada.',
    'R': 'Documento de resumo doutros documentos criados noutras aplicações e gerado nesta aplicação.',
}

# Reference: Manual de Integeração de Software. Comunicação das Faturas á AT

at_tax_exemption_reasons = {
    'M01': 'Artigo 16.º, n.º 6 do CIVA | Artigo 16.º n.º 6, alíneas a) a d) do CIVA',
    'M02': 'Artigo 6.º do Decreto-Lei n.º 198/90, de 19 de junho | Artigo 6.º do Decreto-Lei n.º 198/90, '
           'de 19 de junho',
    'M04': 'Isento artigo 13.º do CIVA | Artigo 13.º do CIVA',
    'M05': 'Isento artigo 14.º do CIVA | Artigo 14.º do CIVA',
    'M06': 'Isento artigo 15.º do CIVA | Artigo 15.º do CIVA',
    'M07': 'Isento artigo 9.º do CIVA | Artigo 9.º do CIVA',
    'M08': '<obsoleto> IVA - Autoliquidação',
    'M09': 'IVA - não confere direito a dedução | Artigo 62.º alínea b) do CIVA',
    'M10': 'IVA - regime isento | Artigo 57.º do CIVA',
    'M11': 'Regime particular do tabaco | Decreto-Lei n.º 348/85, de 22 de agosto',
    'M12': 'Regime da margem de lucro - Aquisições de viagens | Decreto-Lei n.º 211/85, de 3 de julho',
    'M13': 'Regime da margem de lucro - Bens em segunda mão | Decreto-Lei n.º 199/96, de 18 de outubro',
    'M14': 'Regime da margem de lucro - Objetos de arte | Decreto-Lei n.º 199/96, de 18 de outubro',
    'M15': 'Regime da margem de lucro - Objetos de coleção e antiguidades | Decreto-Lei n.º 199/96, de 18 de outubro',
    'M16': 'Isento artigo 14.º do RITI | Artigo 14.º do RITI',
    'M19': 'Outras isenções | Isenções temporárias determinadas em diploma próprio',
    'M20': 'IVA - regime forfetário | Artigo 59.º-D, n.º 2 do CIVA',
    'M21': 'IVA - não confere direito a dedução | Artigo 72.º n.º 4 do CIVA',
    'M25': 'Mercadorias a consignação | Artigo 38.º n.º 1 alínea a) do CIVA',
    'M26': 'Isenção de IVA com direito a dedução no cabaz alimentar | Lei n.º 17/2023, de 14 de abril',
    'M30': 'IVA - autoliquidação | Artigo 2.º, n.º 1 alínea i) do CIVA',
    'M31': 'IVA - autoliquidação | Artigo 2.º, n.º 1 alínea j) do CIVA',
    'M32': 'IVA - autoliquidação | Artigo 2.º, n.º 1 alínea l) do CIVA',
    'M33': 'IVA - autoliquidação | Artigo 2.º, n.º 1 alínea m) do CIVA',
    'M34': 'IVA - autoliquidação | Artigo 2.º, n.º 1 alínea n) do CIVA',
    'M40': 'IVA - autoliquidação | Artigo 6.º, n.º 6 alínea a), do CIVA, a contrario',
    'M41': 'IVA - autoliquidação | Artigo 8.º, n.º 3 do RITI',
    'M42': 'IVA - autoliquidação | Decreto-Lei n.º 212/2004, de 16 de janeiro',
    'M43': 'IVA - autoliquidação | Decreto-Lei n.º 362/99, de 16 de setembro',
    'M99': 'Não sujeito ou não tributado | Outras situações de não liquidação do imposto '
           '(Exemplos: artigo 2.º, n.º 2; artigo 3.º, n.ºs 4, 6 e 7; artigo 4.º, n.º 5, todos do CIVA)'
}

AT_TAX_ACCOUNTING_BASES_VALS = list(at_tax_accounting_bases.items())
AT_TAX_TYPES_VALS = list(at_tax_types.items())
AT_TAX_REGIONS_VALS = list(at_tax_regions.items())
AT_TAX_CODES_VALS = list(at_tax_codes.items())
AT_SOURCE_BILLINGS_VALS = list(at_source_billings.items())
AT_INVOICE_STATUSES_VALS = list(at_invoice_statuses.items())
AT_INVOICE_TYPES_VALS = list(at_invoice_types.items())
AT_PRODUCT_TYPES_VALS = list(at_product_types.items())
AT_PAYMENT_TYPES_VALS = list(at_payment_types.items())
AT_PAYMENT_STATUSES_VALS = list(at_payment_statuses.items())
AT_SOURCE_PAYMENTS_VALS = list(at_source_payments.items())
AT_PAYMENT_MECHANISMS_VAlS = list(at_payment_mechanisms.items())
AT_MOVEMENT_TYPES_VALS = list(at_movement_types.items())
AT_MOVEMENT_STATUS_VALS = list(at_movement_statuses.items())
AT_TAX_EXEMPTION_REASON_VALS = list(at_tax_exemption_reasons.items())

