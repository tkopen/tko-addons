from odoo import api, fields, models, _
from odoo.exceptions import Warning, ValidationError

class AccountMoveReversal(models.TransientModel):
    _inherit = 'account.move.reversal'

    # Removed 'refund' option
    filter_refund = fields.Selection([
        ('cancel', 'Cancel: create credit note and reconcile'),
        ('modify', 'Modify: create credit note, reconcile and create a new draft invoice')
    ], default='cancel', string='Refund Method', required=True, help='Refund base on this type. You can not Modify and Cancel if the invoice is already reconciled')

    def reverse_moves(self):
        # ANSER BEGIN
        if not self.move_ids.filtered(lambda m: m.move_type not in ['in_invoice', 'in_refund']):
            return super(AccountMoveReversal, self).reverse_moves()
        # ANSER END
        move_obj = self.env['account.move']
        context = dict(self._context or {})
        action = super(AccountMoveReversal, self).reverse_moves()
        for form in self:
            for move in move_obj.browse(context.get('active_ids')):
                if move.payment_state in ('partial'):
                    raise ValidationError(_("You can not create credit note for partially paid invoices."))
                for record in self.new_move_ids:
                    if record.einvoice:
                        record.emit_invoice()
        return action

    @api.model
    def default_get(self, fields):
        res = super(AccountMoveReversal, self).default_get(fields)
        active_ids = self._context.get('active_ids')
        for move in self.env['account.move'].browse(active_ids):
            res['refund_method'] = 'cancel'
            res['reason'] = _('Invalid values #%s') % move.name
        return res


    def _prepare_default_reversal(self, move):
        # OVERRIDE
        values = super()._prepare_default_reversal(move)
        if move.einvoice:
            values.update({
                'auto_post': 'no',
            })
        return values