import json
import random
import logging
import uuid
import urllib.parse

from dateutil.relativedelta import relativedelta
from datetime import datetime
from odoo import fields, tools
from odoo.tests import tagged, common
from odoo.tools import float_compare
import xmlrpc.client

from . import tests_common as tests_common

_logger = logging.getLogger(__name__)

# HOWTO:
# Run this tests starting Odoo with:
# [...] -d <database> -u <module> --dev xml --workers 0 --test-tags faturaqui

@tagged('-at_install', 'post_install', 'faturaqui')
class TestFaturAqui(common.HttpCase):

    # Runs once when the tests begin
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        tests_common._init_test_dbs(cls)

    # Runs once when all of the tests end
    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        tests_common._drop_test_dbs(cls)

    def test_faturaqui_01(self):
        _logger.info('================================================================================')
        _logger.info('================================================================================')
        _logger.info('================================ CREATE INVOICE ================================')
        _logger.info('================================================================================')
        _logger.info('================================================================================')

        common = xmlrpc.client.ServerProxy(tests_common.SERVER_URL + '/xmlrpc/2/common')
        _logger.info("Version: ", common.version())
        rpc_db = xmlrpc.client.ServerProxy(tests_common.SERVER_URL + '/xmlrpc/2/db')

        uid = common.login(tests_common.TEST_FA_SERVER_DATABASE_NAME, tests_common.TEST_DATABASE_ADMIN, tests_common.TEST_DATABASE_ADMIN_PWD)
        _logger.info("UID: %s", uid)
        models = xmlrpc.client.ServerProxy(tests_common.SERVER_URL + '/xmlrpc/2/object')

        _logger.info('Creating VAT 23% Invoice...')
        invoice1_id = models.execute_kw(tests_common.TEST_FA_CLIENT_DATABASE_NAME, uid, tests_common.TEST_DATABASE_ADMIN_PWD, 'account.move', 'create', [{
            'partner_id': 7, # Test Client
            'partner_shipping_id': 7, # Test Client
            'journal_id': 1,
            'move_type': 'out_invoice',
            'invoice_line_ids': [[0, 0, {
                # Payment Terms
                'journal_id': 1,
                'sequence': 12000,
                'account_id': 9,
                'account_root_id': 50049,
                'display_type': 'payment_term',
                'date_maturity': datetime.utcnow().date(),
                'debit': 100,
                'credit': 0,
                'balance': 100,
                'amount_currency': 100,
                'amount_residual': 100,
                'amount_residual_currency': 100,
                'quantity': 0,
                'price_unit': 0,
                'price_subtotal': 0,
                'price_total': 0,
                'tax_tag_invert': False,
                'reconciled': False
                }],
                [0, 0, {
                # Product
                'product_id': 1, # SaaS Service
                'account_id': 290,
                'account_root_id': 50049,
                'journal_id': 1,
                'debit': 100,
                'balance': -100,
                'amount_currency': -100,
                'price_unit': 100,
                'price_subtotal': 100,
                'price_total': 100,
                'tax_tag_invert': True,
                'reconciled': False,
                'display_type': 'product',
                'at_unit_price': 100
            }]],
            'emit_einvoice': False,
            'amount_untaxed': 100,
            'amount_total': 100,
            'amount_residual': 100,
            'amount_untaxed_signed': 100,
            'amount_total_signed': 100,
            'amount_total_in_currency_signed': 100,
            'amount_residual_signed': 100,
            'quick_edit_total_amount': 0,
            'always_tax_exigible': False,
            'to_check': False,
            'posted_before': False
        }])
        _logger.info('VAT 23% Invoice created successfully...')

        _logger.info('Confirming VAT 23% Invoice...')
        post_id = models.execute_kw(tests_common.TEST_FA_CLIENT_DATABASE_NAME, uid, tests_common.TEST_DATABASE_ADMIN_PWD, 'account.move', 'action_post', [invoice1_id])
        _logger.info('VAT 23% Invoice confirmed successfully...')

        _logger.info('Validate VAT 23% Invoice AT certification fields are correctly filled...')
        validate_invoice_id = models.execute_kw(tests_common.TEST_FA_CLIENT_DATABASE_NAME, uid, tests_common.TEST_DATABASE_ADMIN_PWD, 'account.move', 'search_read', [[('id', '=', invoice1_id)],  ['name', 'fa_invoice_no', 'fa_hash_code', 'fa_invoice_type', 'server_reference']])[0]

        _logger.info('Check name field: %s', validate_invoice_id['name'])
        self.assertTrue(validate_invoice_id['name'])
        _logger.info('Check fa_invoice_no field: %s', validate_invoice_id['fa_invoice_no'])
        self.assertTrue(validate_invoice_id['fa_invoice_no'])
        _logger.info('Check fa_hash_code field: %s' % validate_invoice_id['fa_hash_code'])
        self.assertTrue(validate_invoice_id['fa_hash_code'])
        _logger.info('Check fa_invoice_type field: %s' % validate_invoice_id['fa_invoice_type'])
        self.assertTrue(validate_invoice_id['fa_invoice_type'])
        _logger.info('Check server_reference field: %s' % validate_invoice_id['server_reference'])
        self.assertTrue(validate_invoice_id['server_reference'])

        _logger.info('VAT 23% Invoice AT certification fields check complete...')
