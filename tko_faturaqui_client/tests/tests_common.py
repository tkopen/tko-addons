import logging

from dateutil.relativedelta import relativedelta
from odoo import fields
from odoo.exceptions import AccessError
from odoo.http import dispatch_rpc
import xmlrpc.client

_logger = logging.getLogger(__name__)

# Odoo server credentials to create and drop databases
SERVER_URL = 'http://localhost:8069'
SERVER_ADMIN_PASSWORD = '4ns3r#'

TEST_DATABASE_ADMIN = 'admin'
TEST_DATABASE_ADMIN_PWD = 'admin'
TEST_FA_SERVER_DATABASE_NAME = 'fat.16.fmag.anser.dev'
TEST_FA_CLIENT_DATABASE_NAME = 'fa.16.fmag.anser.dev'

# If set to true creates a new instance of the FaturAqui Server. If set to False uses an existing instance.
CREATE_FA_SERVER_DATABASE = False

# Do a cleanup on the databases created during the tests
DELETE_TEST_DATABASES_AFTER_FINISH = False

# Global Variables
TEST_PARTNER_CLIENT_TOKEN = False
PARTNER_ID = False
PRODUCT1_ID = False
PRODUCT2_ID = False

def _create_new_db(SERVER_ADMIN_PASSWORD, db_name, TEST_DATABASE_ADMIN, TEST_DATABASE_ADMIN_PWD, LANG, COUNTRY_CODE):
    _logger.info('Creating new database %s...' % db_name)
    dispatch_rpc('db', 'create_database', [SERVER_ADMIN_PASSWORD, db_name, False, LANG, TEST_DATABASE_ADMIN, TEST_DATABASE_ADMIN_PWD, COUNTRY_CODE, False])

def _init_test_dbs(cls):
    _logger.info('INITIALIZING DATABASES...')

    common = xmlrpc.client.ServerProxy(SERVER_URL + '/xmlrpc/2/common')
    _logger.info("Version: ", common.version())
    rpc_db = xmlrpc.client.ServerProxy(SERVER_URL + '/xmlrpc/2/db')

    if CREATE_FA_SERVER_DATABASE:
        _logger.info('INITIALIZING FaturAqui SERVER DATABASE...')

        try:
            if TEST_FA_SERVER_DATABASE_NAME in rpc_db.list():
                _logger.info('Database %s already exists. Deleting it first...' % TEST_FA_SERVER_DATABASE_NAME)
                dispatch_rpc('db', 'drop', [SERVER_ADMIN_PASSWORD, TEST_FA_SERVER_DATABASE_NAME])
                _logger.info('Database %s deleted' % TEST_FA_SERVER_DATABASE_NAME)

            _logger.info('Creating new database %s...' % TEST_FA_SERVER_DATABASE_NAME)
            _create_new_db(SERVER_ADMIN_PASSWORD, TEST_FA_SERVER_DATABASE_NAME, TEST_DATABASE_ADMIN, TEST_DATABASE_ADMIN_PWD, 'en_US', 'us')

            _logger.info('Logging into the newly created database')
            uid = common.login(TEST_FA_SERVER_DATABASE_NAME, TEST_DATABASE_ADMIN, TEST_DATABASE_ADMIN_PWD)
            _logger.info("UID: %s", uid)
            models = xmlrpc.client.ServerProxy(SERVER_URL + '/xmlrpc/2/object')

            _logger.info('Installing FaturAqui Server addon in %s...' % TEST_FA_SERVER_DATABASE_NAME)
            module_id = models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.module.module', 'search', [[('name', '=', 'tko_faturaqui_server')]])
            models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.module.module', 'button_immediate_install', [module_id[0]])
            _logger.info('FaturAqui Server addon installed successfuly in %s...' % TEST_FA_SERVER_DATABASE_NAME)

            _logger.info('Setting up FaturAqui Server database %s...' % TEST_FA_SERVER_DATABASE_NAME)
            models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'faturaqui.configuration', 'prepare_server', [[1]])
            _logger.info('FaturAqui Server database %s setup successfuly' % TEST_FA_SERVER_DATABASE_NAME)

            _logger.info('Creating test client in FaturAqui Server database %s...' % TEST_FA_SERVER_DATABASE_NAME)
            ft_sequence_id = models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.sequence', 'create', [{
                'name': 'FT-TEST',
                'code': 'FTTEST',
                'atcud': '111111',
                'prefix': 'FT 2023/',
                'padding': 6
            }])
            nc_sequence_id = models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.sequence', 'create', [{
                'name': 'NC-TEST',
                'code': 'NCTEST',
                'atcud': '222222',
                'prefix': 'NC 2023/',
                'padding': 6
            }])

            partner_id = models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'res.partner', 'create', [{
                'company_type': 'company',
                'name': 'Cliente de Teste, Lda',
                'street': 'Rua do Cliente de Teste',
                'street2': 'N 1',
                'city': 'Lisboa',
                'zip': '1234-123',
                'country_id': 183,
                'vat': 'PT999999990',
                'is_client': True,
                'tax_entity': 'PT999999990',
                'ft_sequence_id': ft_sequence_id,
                'nc_sequence_id': nc_sequence_id,
                'email': 'noemail@noemail.com'
            }])
            models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'res.partner', 'generate_client_token', [[partner_id]])

            TEST_PARTNER_CLIENT_TOKEN = models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'res.partner', 'search_read', [[('id', '=', partner_id)],  ['client_token']])[0]['client_token']

            _logger.info('Test client created in FaturAqui Server database %s with client_token = %s...' % (TEST_FA_SERVER_DATABASE_NAME, TEST_PARTNER_CLIENT_TOKEN))

            _logger.info('Freezing web.base.url in database %s...' % TEST_FA_SERVER_DATABASE_NAME)
            config_parameter_id = models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.config_parameter', 'search', [[('key', '=', 'web.base.url')]])
            if config_parameter_id:
                _logger.info('Setting web.base.url to %s' % ('https://%s' % TEST_FA_SERVER_DATABASE_NAME))
                models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.config_parameter', 'write', [[config_parameter_id[0]], { 'value': 'https://%s' % TEST_FA_SERVER_DATABASE_NAME}])
            else:
                _logger.error('Key web.base.url not found!')
            models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.config_parameter', 'create', [{
                'key': 'web.base.url.freeze',
                'value': True
            }])
            _logger.info('Freezing web.base.url in database %s done!' % TEST_FA_SERVER_DATABASE_NAME)

            _logger.info('FaturAqui SERVER DATABASE INITIALIZED SUCESSFULLY')
        except Exception as e:
            _logger.info('FaturAqui SERVER DATABASE DID NOT INITIALIZE SUCESSFULLY: %s' % str(e))
            exit(1)
    else:
        _logger.info('Logging into the existing FaturAqui database %s' % TEST_FA_SERVER_DATABASE_NAME)
        uid = common.login(TEST_FA_SERVER_DATABASE_NAME, TEST_DATABASE_ADMIN, TEST_DATABASE_ADMIN_PWD)
        _logger.info("UID: %s", uid)
        models = xmlrpc.client.ServerProxy(SERVER_URL + '/xmlrpc/2/object')

        TEST_PARTNER_CLIENT_TOKEN = models.execute_kw(TEST_FA_SERVER_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'res.partner', 'search_read', [[('vat', '=', 'PT999999990')],  ['client_token']])[0]['client_token']
        _logger.info('Retrieved Test client client_token = %s...' % TEST_PARTNER_CLIENT_TOKEN)

    _logger.info('INITIALIZING FaturAqui CLIENT DATABASE...')
    try:
        if TEST_FA_CLIENT_DATABASE_NAME in rpc_db.list():
            _logger.info('Database %s already exists. Deleting it first...' % TEST_FA_CLIENT_DATABASE_NAME)
            dispatch_rpc('db', 'drop', [SERVER_ADMIN_PASSWORD, TEST_FA_CLIENT_DATABASE_NAME])
            _logger.info('Database %s deleted' % TEST_FA_CLIENT_DATABASE_NAME)

        _logger.info('Creating new database %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        _create_new_db(SERVER_ADMIN_PASSWORD, TEST_FA_CLIENT_DATABASE_NAME, TEST_DATABASE_ADMIN, TEST_DATABASE_ADMIN_PWD, 'pt_PT', 'pt')

        _logger.info('Logging into the newly created database')
        uid = common.login(TEST_FA_CLIENT_DATABASE_NAME, TEST_DATABASE_ADMIN, TEST_DATABASE_ADMIN_PWD)
        _logger.info("UID: %s", uid)
        models = xmlrpc.client.ServerProxy(SERVER_URL + '/xmlrpc/2/object')

        _logger.info('Installing FaturAqui Client addon in %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        module_id = models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.module.module', 'search', [[('name', '=', 'tko_faturaqui_client')]])
        models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.module.module', 'button_immediate_install', [module_id[0]])
        _logger.info('FaturAqui Client addon installed successfully in %s...' % TEST_FA_CLIENT_DATABASE_NAME)

        _logger.info('Installing Contacts addon in %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        module_id = models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.module.module', 'search', [[('name', '=', 'contacts')]])
        models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'ir.module.module', 'button_immediate_install', [module_id[0]])
        _logger.info('Contacts addon installed successfully in %s...' % TEST_FA_CLIENT_DATABASE_NAME)

        _logger.info('Reactivating the Invoicing Journal in %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'account.journal', 'write', [[1], {
            'active': True,
            'at_type_out_invoice': 'FT',
            'refund_code': 'NC',
            'code': 'FT'
        }])
        _logger.info('Invoicing Journal reactivated successfully in %s...' % TEST_FA_CLIENT_DATABASE_NAME)

        _logger.info('Connecting FaturAqui Client to FaturAqui Server in %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'faturaqui', 'write', [[1], {
            'server_url': 'https://%s' % TEST_FA_SERVER_DATABASE_NAME,
            'client_token': TEST_PARTNER_CLIENT_TOKEN,
            'default_invoice_at_self_billing_indicator': '0'
        }])
        _logger.info('FaturAqui Client connected to FaturAqui Server successfully in %s...' % TEST_FA_CLIENT_DATABASE_NAME)

        _logger.info('Updating Company data in %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        partner_id = models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'res.company', 'write', [[1], {
            'name': 'Empresa de Teste, Lda',
            'street': 'Rua da Empresa de Teste',
            'street2': 'N 1',
            'city': 'Lisboa',
            'zip': '1234-123',
            'country_id': 183,
            'vat': 'PT999999990',
            'company_registry': 'PT999999990',
            'email': 'noemail@noemail.com'
        }])
        _logger.info('Company data updated successfully in %s...' % TEST_FA_CLIENT_DATABASE_NAME)

        _logger.info('Creating Test Client in %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        PARTNER_ID = models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'res.partner', 'create', [{
            'company_type': 'company',
            'name': 'Cliente de Teste, Lda',
            'street': 'Rua do Cliente de Teste',
            'street2': 'N 1',
            'city': 'Lisboa',
            'zip': '1234-123',
            'country_id': 183,
            'vat': 'PT999999990',
            'email': 'noemail@noemail.com'
        }])
        _logger.info('Test Client created successfully in %s...' % TEST_FA_CLIENT_DATABASE_NAME)

        _logger.info('Creating Test Products in %s...' % TEST_FA_CLIENT_DATABASE_NAME)
        PRODUCT1_ID = models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'product.template', 'create', [{
            'name': 'SaaS Service',
            'detailed_type': 'service',
            'list_price': 10,
            'default_code': '2023SAAS',
            'taxes_id': [[6, False, [1]]], # 23% VAT
            'at_product_type': 'S'
        }])

        PRODUCT2_ID = models.execute_kw(TEST_FA_CLIENT_DATABASE_NAME, uid, TEST_DATABASE_ADMIN_PWD, 'product.template', 'create', [{
            'name': 'Vatless Service',
            'detailed_type': 'service',
            'list_price': 100,
            'default_code': '2023VLESS',
            'taxes_id': [[6, False, [10]]], # 0% VAT
            'at_product_type': 'S'
        }])

        _logger.info('Test Products created successfully in %s...' % TEST_FA_CLIENT_DATABASE_NAME)


        _logger.info('FaturAqui CLIENT DATABASE INITIALIZED SUCESSFULLY')
    except Exception as e:
        _logger.info('FaturAqui CLIENT DATABASE DID NOT INITIALIZE SUCESSFULLY: %s' % str(e))
        exit(2)


def _drop_test_dbs(cls):
    if DELETE_TEST_DATABASES_AFTER_FINISH:
        _logger.info('DELETING TEST DATABASES...')
        _logger.info('%s' % TEST_DATABASES)

        for db in TEST_DATABASES:
            dispatch_rpc('db', 'drop', [SERVER_ADMIN_PASSWORD, db['name']])
